package edu.ntnu.stud.cardgame;

import edu.ntnu.stud.cardgame.controller.ButtonController;
import edu.ntnu.stud.cardgame.view.HomeFrame;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * The main class for the Card Game application. This class sets up the primary stage and the
 * home frame, and sets up the button actions for the home frame.
 * This is where the application runs from.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class CardGame extends Application {

  /**
   * The main method for the Card Game application. This method launches the application.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * The start method for the Card Game application. This method sets up the primary stage and the
   * home frame, and sets up the button actions for the home frame.
   *
   * @param primaryStage The primary stage for the application.
   */
  @Override
  public void start(Stage primaryStage) {

    primaryStage.setTitle("Card Game");
    primaryStage.getIcons().add(new Image(
        "file:src/main/resources/edu/ntnu/stud/cardgame/CardGameLogo.png"));

    HomeFrame homeFrame = new HomeFrame();
    ButtonController btnController = new ButtonController(homeFrame);

    setupButtonActions(homeFrame, btnController);

    primaryStage.setScene(new Scene(homeFrame, 1000, 800));
    primaryStage.show();
  }

  /**
   * Sets up the button actions for the home frame.
   *
   * @param homeFrame     The home frame for the application.
   * @param btnController The button controller for the application.
   */
  private void setupButtonActions(HomeFrame homeFrame, ButtonController btnController) {
    homeFrame.getButtons().actionOnDealButton(e -> btnController.actionOnDealButton());
    homeFrame.getButtons().actionOnCheckButton(e -> btnController.actionOnCheckButton());
    homeFrame.getButtons().actionOnQuitButton(e -> btnController.actionOnQuitButton());
  }
}
