package edu.ntnu.stud.cardgame.controller;

import edu.ntnu.stud.cardgame.util.ImageLoadingException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.image.Image;

/**
 * Class for importing images from the file system and storing them in a map for easy access.
 * The class is used to import images of playing cards for the card game application.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class ImageImporter {

  // Class fields
  private static final char[] SUITS = {'H', 'D', 'S', 'C'};
  private final String path;
  Map<String, Image> images;

  /**
   * Constructor for the ImageImporter class.
   * Initializes the class fields and imports the images from the file system.
   *
   * @param path The path to the directory containing the images.
   */
  public ImageImporter(String path) {
    this.path = path;
    this.images = new HashMap<>();
    this.initialize();
  }

  /**
   * Method for importing images from the file system and storing them in a map.
   */
  public void initialize() {
    List<String> files = new ArrayList<>();

    for (char suit : SUITS) {
      for (int j = 1; j <= 13; j++) {
        files.add(path + suit + j + ".png");
      }
    }

    for (String file : files) {
      try (InputStream stream = getClass().getResourceAsStream(file)) {
        if (stream == null) {
          throw new ImageLoadingException("Failed to load image file: " + file);
        }
        Image image = new Image(stream);
        images.put(getFilenameWithoutExtension(file), image);
      } catch (Exception e) {
        throw new ImageLoadingException("Failed to load image file: " + file, e);
      }
    }
  }

  /**
   * Private method for extracting the filename from a path without the file extension.
   * Used to create keys for the image map.
   *
   * @param path The path to the file.
   * @return The filename without the file extension.
   */
  private String getFilenameWithoutExtension(String path) {
    String file = path.substring(path.lastIndexOf("/") + 1);
    return file.substring(0, file.lastIndexOf("."));
  }

  /**
   * Method for retrieving an image from the map using a key.
   *
   * @param key The key for the image in the map.
   * @return The image corresponding to the key.
   */
  public Image getImage(String key) {
    Image image = images.get(key);
    if (image == null) {
      throw new IllegalArgumentException("No image found for key: " + key);
    }
    return image;
  }
}