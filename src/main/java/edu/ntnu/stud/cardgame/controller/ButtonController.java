package edu.ntnu.stud.cardgame.controller;

import edu.ntnu.stud.cardgame.model.Hand;
import edu.ntnu.stud.cardgame.model.PlayingCard;
import edu.ntnu.stud.cardgame.view.HomeFrame;
import edu.ntnu.stud.cardgame.view.components.CustomButton;
import edu.ntnu.stud.cardgame.view.components.OverlayView;
import java.util.stream.Collectors;
import javafx.application.Platform;

/**
 * Controller class for the buttons in the HomeFrame and their actions.
 * The class is responsible for creating a new deck of cards, dealing cards, and checking the hand
 * when the buttons are pressed.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class ButtonController {

  // Class fields
  private final DeckOfCards deck;
  private Hand hand;
  private final HomeFrame homeFrame;

  /**
   * Constructor for the ButtonController class.
   * Initializes the class fields and creates a new deck of cards.
   *
   * @param homeFrame The HomeFrame object used in the application.
   */
  public ButtonController(HomeFrame homeFrame) {
    ImageImporter imageImporter = new ImageImporter("/edu/ntnu/stud/cardgame/images/");
    this.deck = new DeckOfCards(imageImporter);
    this.homeFrame = homeFrame;
  }

  /**
   * Method for dealing cards from the deck and updating the
   * corresponding frames.
   *
   * @param n The number of cards to deal.
   */
  private void dealCards(int n) {
    this.hand = new Hand(deck.dealHand(n));
    homeFrame.getCheckedHand().clear();
    homeFrame.getCardFrame().clear();
    homeFrame.getCardFrame().addCards(hand);
  }

  /**
   * Private method for creating a deal button with a given number of cards to deal.
   *
   * @param n The number of cards to deal.
   * @param overlayView The overlay view to add the button to.
   * @return The created button.
   */
  private CustomButton createDealButton(int n, OverlayView overlayView) {
    CustomButton button = new CustomButton("Deal " + n);
    button.setOnAction(e -> {
      dealCards(n);
      homeFrame.getChildren().remove(overlayView.getOverlay());
    });
    return button;
  }

  /**
   * Decides the action to be taken when the deal button is pressed.
   */
  public void actionOnDealButton() {
    OverlayView overlayView = new OverlayView();
    overlayView.addButtons(createDealButton(5, overlayView),
        createDealButton(10, overlayView), createDealButton(15, overlayView));

    overlayView.getOverlay().setOnMouseClicked(e ->
        homeFrame.getChildren().remove(overlayView.getOverlay()));

    homeFrame.getChildren().add(overlayView.getOverlay());
  }

  /**
   * Decides the action to be taken when the check button is pressed.
   */
  public void actionOnCheckButton() {
    homeFrame.getCheckedHand().clear();
    homeFrame.getCheckedHand().setSumField(Integer.toString(hand.sumOfFaces()));

    String hearts = hand.getCardsOfHearts().isEmpty() ? "No Hearts" :
        hand.getCardsOfHearts().stream()
            .map(PlayingCard::getAsString)
            .collect(Collectors.joining(", "));
    homeFrame.getCheckedHand().setHeartsField((hearts));

    String queenOfSpades = hand.hasQueenOfSpades() ? "Yes" : "No";
    String flush = hand.isFlush() ? "Yes" : "No";
    homeFrame.getCheckedHand().setQueenOfSpadesField(queenOfSpades);
    homeFrame.getCheckedHand().setFlushField(flush);
  }

  /**
   * Decides the action to be taken when the quit button is pressed.
   */
  public void actionOnQuitButton() {
    Platform.exit();
  }
}
