package edu.ntnu.stud.cardgame.controller;

import edu.ntnu.stud.cardgame.model.PlayingCard;
import java.util.Collection;
import java.util.Random;

/**
 * A deck of playing cards. The deck is initialized with 52 cards, and cards can be dealt from the
 * deck.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class DeckOfCards {

  // Class fields
  private final PlayingCard[] cards;
  private final Random random;
  private static final char[] SUITS = {'H', 'D', 'S', 'C'};

  /**
   * Constructs a new deck of cards. Takes an image importer as an argument to import images for the
   * cards used in the game to create the cards with their images.
   *
   * @param imageImporter an image importer to import images for the cards used in the game.
   */
  public DeckOfCards(ImageImporter imageImporter) {
    this.cards = new PlayingCard[52];
    this.random = new Random();

    for (int i = 0; i < SUITS.length; i++) {
      for (int j = 1; j <= 13; j++) {
        String key = String.format("%s%d", SUITS[i], j);
        cards[i * 13 + j - 1] = new PlayingCard(SUITS[i], j, imageImporter.getImage(key));
      }
    }
  }

  /**
   * Deals a hand of n cards from the deck. The cards are chosen randomly from the deck, and the
   * cards are removed from the deck.
   *
   * @param n the number of cards to deal.
   * @return a collection of n cards.
   * @throws IllegalArgumentException if n is less than 0 or greater than 52.
   */
  public Collection<PlayingCard> dealHand(int n) {
    if (n < 0 || n > 52) {
      throw new IllegalArgumentException("Invalid number of cards: " + n);
    }

    return random.ints(0, cards.length)
        .distinct()
        .limit(n)
        .mapToObj(i -> cards[i])
        .toList();
  }
}
