package edu.ntnu.stud.cardgame.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class representing a hand of playing cards.
 * The class is used to represent a hand of playing cards and contains methods for checking the
 * hand for specific combinations. This class is a Record class because it is a simple data
 * container with no behavior.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public record Hand(Collection<PlayingCard> cards) {

  /**
   * Method for calculating the sum of the faces of the cards in the hand.
   *
   * @return The sum of the faces of the cards in the hand.
   */
  public int sumOfFaces() {
    return cards.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  /**
   * Method for getting all the cards of hearts in the hand.
   *
   * @return A collection of all the cards of hearts in the hand.
   */
  public Collection<PlayingCard> getCardsOfHearts() {
    Collection<PlayingCard> hearts = new ArrayList<>();
    for (PlayingCard card : cards) {
      if (card.getSuit() == 'H') {
        hearts.add(card);
      }
    }
    return hearts;
  }

  /**
   * Method for checking if the hand contains the queen of spades.
   *
   * @return True if the hand contains the queen of spades, false otherwise.
   */
  public boolean hasQueenOfSpades() {
    for (PlayingCard card : cards) {
      if (card.getSuit() == 'S' && card.getFace() == 12) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method for checking if the hand contains a flush.
   *
   * @return True if the hand contains a flush, false otherwise.
   */
  public boolean isFlush() {
    char suit = cards.iterator().next().getSuit();
    for (PlayingCard card : cards) {
      if (card.getSuit() != suit) {
        return false;
      }
    }
    return true;
  }
}
