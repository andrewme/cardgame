package edu.ntnu.stud.cardgame.util;

/**
 * Custom exception class for handling image loading exceptions.
 * The class is used to handle exceptions when importing images from the file system.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class ImageLoadingException extends RuntimeException {

  /**
   * Constructor for the ImageLoadingException class.
   *
   * @param message The message to be displayed when the exception is thrown.
   */
  public ImageLoadingException(String message) {
    super(message);
  }

  /**
   * Constructor for the ImageLoadingException class.
   *
   * @param message The message to be displayed when the exception is thrown.
   * @param cause The cause of the exception.
   */
  public ImageLoadingException(String message, Throwable cause) {
    super(message, cause);
  }
}