package edu.ntnu.stud.cardgame.view.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;

/**
 * A class for the buttons in the game used to deal, check and quit the game.
 * The class extends VBox and contains three buttons.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class Buttons extends VBox {

  // The buttons used in the game.
  private final CustomButton deal;
  private final CustomButton check;
  private final CustomButton quit;

  /**
   * Constructor for the Buttons class.
   * Sets the padding for the VBox and adds the buttons to the VBox.
   * Also sets the margins for the buttons.
   */
  public Buttons() {
    this.setPadding(new Insets(15));

    this.deal = new CustomButton("Deal Hand");
    this.check = new CustomButton("Check Hand");
    this.quit = new CustomButton("Quit Game");

    this.getChildren().addAll(deal, check, quit);

    VBox.setMargin(check, new Insets(60, 0, 0, 0));
    VBox.setMargin(quit, new Insets(240, 0, 0, 0));
  }

  /**
   * Method for setting the action on the deal button.
   *
   * @param event The event to be set on the deal button.
   */
  public void actionOnDealButton(EventHandler<ActionEvent> event) {
    deal.setOnAction(event);
  }

  /**
   * Method for setting the action on the check button.
   *
   * @param event The event to be set on the check button.
   */
  public void actionOnCheckButton(EventHandler<ActionEvent> event) {
    check.setOnAction(event);
  }

  /**
   * Method for setting the action on the quit button.
   *
   * @param event The event to be set on the quit button.
   */
  public void actionOnQuitButton(EventHandler<ActionEvent> event) {
    quit.setOnAction(event);
  }
}
