package edu.ntnu.stud.cardgame.view.components;

/**
 * A custom button with a name and styling for the game.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class CustomButton extends javafx.scene.control.Button {

  // The name of the button
  private final String name;

  /**
   * Creates a new custom button with the given name.
   *
   * @param name The name of the button
   */
  public CustomButton(String name) {
    this.name = name;
    this.setText(name);
    this.getStyleClass().add("button");

    this.setMinSize(160, 60);
  }

  /**
   * Returns the name of the button.
   *
   * @return The name of the button
   */
  public String getName() {
    return name;
  }
}
