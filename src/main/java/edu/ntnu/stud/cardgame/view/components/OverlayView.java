package edu.ntnu.stud.cardgame.view.components;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * A view for an overlay with buttons.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class OverlayView {

  // The overlay
  private final StackPane overlay;

  /**
   * Creates a new overlay view.
   */
  public OverlayView() {
    this.overlay = new StackPane();
    this.overlay.setStyle("-fx-background-color: rgba(0,0,0,0.49);");
  }

  /**
   * Adds buttons to the overlay.
   *
   * @param buttons The buttons to add.
   */
  public void addButtons(Button... buttons) {
    VBox buttonBox = new VBox();
    buttonBox.getStyleClass().add("content");
    buttonBox.getChildren().addAll(buttons);
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.setMaxSize(200, 200);
    buttonBox.setSpacing(10);
    this.overlay.getChildren().add(buttonBox);
  }

  /**
   * Get the overlay.
   *
   * @return The overlay.
   */
  public StackPane getOverlay() {
    return this.overlay;
  }
}