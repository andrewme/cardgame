package edu.ntnu.stud.cardgame.view;

import edu.ntnu.stud.cardgame.view.components.Buttons;
import edu.ntnu.stud.cardgame.view.components.CardFrame;
import edu.ntnu.stud.cardgame.view.components.ChecksForHand;
import java.util.Objects;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

/**
 * The main frame of the game. Contains the card frame, buttons and checks for hand.
 * This is the main frame of the game where all the other
 * components are added.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class HomeFrame extends StackPane {

  // The card frame, buttons and additional info for the hand.
  private final CardFrame cardFrame;
  private final Buttons buttons;
  private final ChecksForHand checkedHand;

  /**
   * Creates a new HomeFrame. Sets the size and adds the card frame, buttons and checks for hand.
   * Also adds the global stylesheet.
   */
  public HomeFrame() {

    this.setPrefSize(1000, 800);
    this.getStylesheets().add(Objects.requireNonNull(getClass().getResource(
        "/edu/ntnu/stud/cardgame/globals.css")).toExternalForm());

    this.getStyleClass().add("home-frame");

    this.cardFrame = new CardFrame();
    this.buttons = new Buttons();
    this.checkedHand = new ChecksForHand();

    this.getChildren().add(getContent());
  }

  /**
   * Creates the content of the frame. Adds the card frame, buttons and checks for hand.
   *
   * @return The content of the frame.
   */
  private GridPane getContent() {

    GridPane content = new GridPane();
    content.setMaxSize(800, 700);
    content.getStyleClass().add("content");
    content.setAlignment(Pos.CENTER);

    content.add(cardFrame, 0, 0);
    content.add(buttons, 1, 0);
    content.add(checkedHand, 0, 1);

    return content;
  }

  /**
   * Get the buttons.
   *
   * @return The buttons.
   */
  public Buttons getButtons() {
    return buttons;
  }

  /**
   * Get the checked hand.
   *
   * @return The checked hand.
   */
  public ChecksForHand getCheckedHand() {
    return checkedHand;
  }

  /**
   * Get the card frame.
   *
   * @return The card frame.
   */
  public CardFrame getCardFrame() {
    return cardFrame;
  }
}
