package edu.ntnu.stud.cardgame.view.components;

import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * A class to get additional information about a hand.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class ChecksForHand extends GridPane {

  // Class fields
  private final TextField sumField;
  private final TextField heartsField;
  private final TextField queenOfSpadesField;
  private final TextField flushField;

  /**
   * Constructor for the ChecksForHand class. Sets up the grid pane and adds the
   * necessary fields.
   */
  public ChecksForHand() {
    this.setHgap(20);
    this.setVgap(10);

    Text sumLabel = new Text("Sum of faces:");
    Text heartsLabel = new Text("Cards of Hearts:");
    Text queenOfSpadesLabel = new Text("Queen of Spades:");
    Text flushLabel = new Text("Flush:");

    String style = "checks";
    sumLabel.getStyleClass().add(style);
    heartsLabel.getStyleClass().add(style);
    queenOfSpadesLabel.getStyleClass().add(style);
    flushLabel.getStyleClass().add(style);

    sumField = new TextField("");
    heartsField = new TextField("");
    queenOfSpadesField = new TextField("");
    flushField = new TextField("");

    this.add(sumLabel, 0, 0);
    this.add(heartsLabel, 0, 1);
    this.add(queenOfSpadesLabel, 0, 2);
    this.add(flushLabel, 0, 3);

    this.add(sumField, 1, 0);
    this.add(heartsField, 1, 1);
    this.add(queenOfSpadesField, 1, 2);
    this.add(flushField, 1, 3);
  }

  /**
   * Method to set the sum field with the sum of the faces of the hand.
   *
   * @param sum The sum to set.
   */
  public void setSumField(String sum) {
    sumField.setText(sum);
  }

  /**
   * Method to set the hearts field with the number of cards of Hearts in the hand.
   *
   * @param hearts The number of cards of Hearts to set.
   */
  public void setHeartsField(String hearts) {
    heartsField.setText(hearts);
  }

  /**
   * Method to set the Queen of Spades field if the hand includes the Queen of Spades.
   *
   * @param queenOfSpades Yes if the hand includes the Queen of Spades, otherwise No.
   */
  public void setQueenOfSpadesField(String queenOfSpades) {
    queenOfSpadesField.setText(queenOfSpades);
  }

  /**
   * Method to set the flush field if the hand is a flush.
   *
   * @param flush Yes if the hand is a flush, otherwise No.
   */
  public void setFlushField(String flush) {
    flushField.setText(flush);
  }

  /**
   * Method to clear all the fields.
   */
  public void clear() {
    sumField.clear();
    heartsField.clear();
    queenOfSpadesField.clear();
    flushField.clear();
  }
}