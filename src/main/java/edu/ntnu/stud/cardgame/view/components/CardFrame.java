package edu.ntnu.stud.cardgame.view.components;

import edu.ntnu.stud.cardgame.model.Hand;
import edu.ntnu.stud.cardgame.model.PlayingCard;
import javafx.geometry.Pos;
import javafx.scene.layout.TilePane;

/**
 * A frame for displaying cards. This class is a subclass of TilePane.
 * It is used to display a hand of cards in a graphical user interface.
 *
 * @author André Merkesdal
 * @version 1.0
 */
public class CardFrame extends TilePane {

  /**
   * Constructs a new CardFrame object.
   */
  public CardFrame() {
    this.setMinSize(700, 500);
    this.setMaxSize(700, 500);
    this.getStyleClass().add("card-frame");
    this.setAlignment(Pos.CENTER);
  }

  /**
   * Adds the cards from a hand to the CardFrame.
   *
   * @param hand The hand to add cards from.
   */
  public void addCards(Hand hand) {
    hand.cards().stream()
        .map(PlayingCard::getImage)
        .forEach(this.getChildren()::add);
  }

  /**
   * Removes all cards from the CardFrame and clears it.
   */
  public void clear() {
    this.getChildren().clear();
  }
}
